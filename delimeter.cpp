#include <iostream>
#include <vector>
#include <cmath>

void get_dlim(int n, std::vector<int> & divs) {
	int d, dlim;
	
	dlim = sqrt( n );
	for (d = 2; d <= dlim; ++d) {
 	 if (n%d == 0) {
  		 divs.push_back(d);
   		 if ( d != n/d )
    			 divs.push_back(n/d);
 	 }
	}
}

int main(void) {
	
	int 			num;
	std::vector<int>	res;

	while (std::cin >> num) {
		get_dlim(num, res);
		for (int i = 0; i < res.size(); i++) {
			std::cout << res[i--] << std::endl;
			res.erase(res.begin());
		}
	}
	return 0;
}
